- Set up the project
- Add assets and setup level
Player
	-Walk
	-Health
	-Collision
	-Shooting
Enemies
	-Walk
	-Attack Player
	-Die
Debug
	GodMode
	FullHealth
	DisableEnemies
	Spawn Enemies
	
	Player assets: http://opengameart.org/content/animated-top-down-survivor-player
	Level assets: http://www.bigrookgames.com/
	Enemy assets: http://spicy-jamb.com/2012/08/