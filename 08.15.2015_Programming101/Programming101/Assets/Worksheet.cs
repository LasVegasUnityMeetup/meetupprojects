﻿using UnityEngine;
using System.Collections;

public class Worksheet : MonoBehaviour {

	// Use this for initialization
	void Start () {
		// Create a variable for the player called health and set the value to 100
		int health = 100;

		// Have the player take 10 damage
		health = health - 10;

		// Have the player heal with 20 damage
		health += 20;

		// Print the value of the player's current health
		print (health);

		// Delcare the cost of a sword as $1.36
		float sword = 1.36f;

		// Print how much 5 swords would cost
		print (sword * 5);

		//Compute these values
			//"3 % 15: " 
			print ("3 % 15: " + (3 % 5));

			//"13 % 15: "
			print ("13 % 15: " + (13 % 15));

			//"8 % 15: " 
			print ("8 % 15: " + (8 % 15));
			
			//"18 % 15: "
			print ("18 % 15: " + (18 % 15));
			
			//"40 % 15: "
			print ("40 % 15: " + (40 % 15));
				
			//"5 * 3 = " 
			print ("5 * 3: " + (5 * 3));
			
			//"5 / 3 = " 
			print ("5 / 3: " + (5 / 3));
			
			//"5 + 3 = " 
			print ("5 + 3: " + (5 + 3));
			
			//"5 - 3 = " 
			print ("5 - 3: " + (5 - 3));
		
			//"-5 - 3 = " 
			print ("-5 - 3: " + (-5 - 3));
				
		// Delcare two ints using multiple delcarations one as the value 5 and the second as the value 20
		int a = 5, b = 20;

		// Create the variable y and set the value to subtracting the first and second variable and print the value
		int y = a - b;
		print (y);

		// Compute and print the follow formulas:
			// (3 * 7) - (1 / 2) * 16
			print ((3 * 7) - (1 / 2) * 16);

			// 3 * 7 - 1 / 2 * 16
			print (3 * 7 - 1 / 2 * 16);	

			// 1 * -1 + 1 
			print (1 * -1 + 1);	

			// 789 / 789 / 4
			print (789 / 789 / 4);	

	}

	// Make a function to multiple two double values and return the result
	double Multiply( double d1, double d2)
	{
		return d1 * d2;
	}

	// Make a function that subtracts two numbers then adds 1
	float Func(int a, int b)
	{
		int subTotal = a - b;
		return subTotal;
	}
}
