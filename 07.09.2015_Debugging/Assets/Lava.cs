using UnityEngine;
using System.Collections;

public class Lava : MonoBehaviour
{
    #region Inspector Fields
    /// <summary>
    /// Amount of damage the lava does towards the player
    /// </summary>
    [SerializeField]
    private int _damage = 10;
    #endregion

    #region Properties
    /// <summary>
    /// Gets the amount of damage the lava does
    /// </summary>
    public int Damage { get { return _damage; } set { } }
    #endregion
}