using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    #region Fields
    /// <summary>
    /// Reference to the main camera
    /// </summary>
    private Camera _camera;

    /// <summary>
    /// Reference to the player's transform
    /// </summary>
    private Transform _transfrom;

    /// <summary>
    /// Reference to the player's sprite renderer
    /// </summary>
    private SpriteRenderer _playerSpriteRenderer;

    /// <summary>
    /// Last time the gun was fired
    /// </summary>
    private float _lastFired;

    /// <summary>
    /// Last time the player was hit
    /// </summary>
    private float _lastHitTime;

    /// <summary>
    /// Current health value of the player
    /// </summary>
    private int _currentHealth;

    /// <summary>
    /// Current score of the player
    /// </summary>
    private int _score;

    /// <summary>
    /// Debug option if the player can't die
    /// </summary>
    private bool _godMode;

    /// <summary>
    /// Debug amount of damage to increase on each bullet
    /// </summary>
    private int _increaseDamage;
    #endregion

    #region Inspector Fields
    /// <summary>
    /// Inspector field for the player sprite's transform
    /// </summary>
    [SerializeField]
    private Transform _playerSprite;

    /// <summary>
    /// Inspector field for the bullet prefab
    /// </summary>
    [SerializeField]
    private Bullet _bulletPrefab;

    /// <summary>
    /// Inspector field for the spawn location offset from the player to create the Bullet
    /// </summary>
    [SerializeField]
    private Transform _bulletSpawnLocation;

    /// <summary>
    /// Inspector field for the health slider to show the current player health
    /// </summary>
    [SerializeField]
    private Slider _healthSlider;

    /// <summary>
    /// Inspector field for the score text when it kills an enemy
    /// </summary>
    [SerializeField]
    private Text _scoreText;

    /// <summary>
    /// Inspector field for the start health for the player
    /// </summary>
    [SerializeField]
    private int _maxHealth = 100;

    /// <summary>
    /// Inspector field for the movement speed of the player
    /// </summary>
    [SerializeField]
    private float _speed = 10;

    /// <summary>
    /// Inspector field for the rate of fire for the player
    /// </summary>
    [SerializeField]
    private float _rateOfFire = 0.25f;

    /// <summary>
    /// Inspector field for the damage cool down before the player is allowed to get hit
    /// </summary>
    [SerializeField]
    private float _damageCoolDown = 1f;
    #endregion

    #region Properties
    /// <summary>
    /// Gets or sets if the player is in god mode
    /// </summary>
    public bool GodMode
    {
        get { return _godMode; }
        set
        {
            _godMode = value;

            ///If we are in god mode, set our colour to yellow, otherwise white
            if (_playerSpriteRenderer != null)
                _playerSpriteRenderer.color = value ? Color.yellow : Color.white;
        }
    }
    #endregion

    #region Behaviour Override
    /// <summary>
    /// Validate inspector fields and set default values
    /// </summary>
    private void Awake()
    {
        if (_playerSprite != null)
            _playerSpriteRenderer = _playerSprite.GetComponent<SpriteRenderer>();
        else
            Debug.LogError("Player sprite reference not set! GameObject: " + name);

        if (_bulletPrefab == null)
            Debug.LogError("Bullet prefab reference not set! GameObject: " + name);

        if (_bulletSpawnLocation == null)
            Debug.LogError("Bullet spawn location reference not set! GameObject: " + name);

        if (_healthSlider == null)
            Debug.LogError("Health slider reference not set! GameObject: " + name);

        if (_scoreText == null)
            Debug.LogError("score text reference not set! GameObject: " + name);

        _camera = Camera.main;
        _transfrom = transform;
        _currentHealth = _maxHealth;
        _score = 0;
    }

    /// <summary>
    /// Update the player's movement and firing bullets
    /// </summary>
    private void Update()
    {
        if (_transfrom != null)
        {
            float v = Input.GetAxis("Vertical");
            bool fire = Input.GetButton("Fire1");
            Vector3 facingDirection = Vector3.zero;

            if (_playerSprite != null && _camera != null)
            {
                Vector3 worldPosition = _camera.WorldToScreenPoint(_transfrom.position);
                facingDirection = (Input.mousePosition - worldPosition).normalized;
                float angle = Mathf.Atan2(facingDirection.y, facingDirection.x) * Mathf.Rad2Deg;
                _playerSprite.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            }

            _transfrom.position += (facingDirection * v) * _speed * Time.deltaTime;

            if (fire && _lastFired + _rateOfFire <= Time.time)
            {
                Bullet newBullet = Instantiate<Bullet>(_bulletPrefab);

                if (newBullet != null && _bulletSpawnLocation != null)
                {
                    newBullet.transform.position = _bulletSpawnLocation.position;
                    newBullet.Direction = facingDirection;
                    newBullet.Owner = this;
                    newBullet.Damage += _increaseDamage;
                }

                _lastFired = Time.time;
            }
        }
    }

    /// <summary>
    /// If the player hits a lava or enemy object, damage it
    /// </summary>
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Lava"))
        {
            Lava lava = collision.GetComponent<Lava>();

            if (lava != null)
                HurtPlayer(lava.Damage);
        }
        else if (collision.CompareTag("Enemy"))
        {
            Enemy enemy = collision.GetComponent<Enemy>();

            if (enemy != null)
            {
                HurtPlayer(enemy.Damage);
            }
        }
    }

    /// <summary>
    /// If we leave the lava pits, return the colour back to normal
    /// </summary>
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (_playerSpriteRenderer != null && !GodMode)
            _playerSpriteRenderer.color = Color.white;
    }
    #endregion


    #region Health
    /// <summary>
    /// Allow the player to receive damage
    /// </summary>
    /// <param name="damage">amount of damage the player takes</param>
    public void HurtPlayer(int damage)
    {
        if (!GodMode)
        {
            if (_lastHitTime + _damageCoolDown <= Time.time)
            {
                _currentHealth -= damage;

                if (_healthSlider != null)
                    _healthSlider.value = _currentHealth / (float)_maxHealth;

                if (_currentHealth < 0)
                    _currentHealth = 0;

                if (_playerSpriteRenderer != null)
                    _playerSpriteRenderer.color = Color.red;

                _lastHitTime = Time.time;
            }
        }
    }
    #endregion

    #region Score
    /// <summary>
    /// Update the player's current score
    /// </summary>
    public void UpdateScore()
    {
        ++_score;

        if (_scoreText != null)
            _scoreText.text = "Score: " + _score.ToString();
    }
    #endregion

    #region Debug
    /// <summary>
    /// Increase the amount of bullet damage down to each bullet
    /// </summary>
    public void IncreaseBulletDamage()
    {
       _increaseDamage += 5;
    }

    /// <summary>
    /// Update the health to max
    /// </summary>
    public void SetFullHealth()
    {
        _currentHealth = _maxHealth;

        if (_healthSlider != null)
            _healthSlider.value = _currentHealth / (float)_maxHealth;
    }
    #endregion
}