using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour
{
    #region Fields
    /// <summary>
    /// Direction the enemy is going
    /// </summary>
    private Vector3 _direction;

    /// <summary>
    /// Reference to the enemy's transform
    /// </summary>
    private Transform _transform;

    /// <summary>
    /// How much health the enemy currently has
    /// </summary>
    private int _currentHealth;

    /// <summary>
    /// Reference to the sprite render drawing the enemy
    /// </summary>
    private SpriteRenderer _spriteRenderer;
    #endregion

    #region Inspector Fields
    /// <summary>
    /// Inspector field for the amount of damage dealt to the player
    /// </summary>
    [SerializeField]
    private int _damage = 5;

    /// <summary>
    /// Inspector field for how fast the enemy can travel
    /// </summary>
    [SerializeField]
    private float _speed = 2f;

    /// <summary>
    /// Inspector field for the amount of health the enemy starts with
    /// </summary>
    [SerializeField]
    private int _maxHealth = 50;
    #endregion

    #region Properties
    /// <summary>
    /// Gets the amount of damage that can be dealt
    /// </summary>
    public int Damage { get { return _damage; } }
    #endregion

    #region Behaviour Overrides
    /// <summary>
    /// Initialize fields
    /// </summary>
    private void Awake()
    {
        //Set our initial direction to a random direction
        _direction = new Vector2(Random.Range(0f, 1f), Random.Range(0f, 1f)).normalized;

        _transform = transform;
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _currentHealth = _maxHealth;
    }

    /// <summary>
    /// Update the enemy position
    /// </summary>
    private void Update()
    {
        if (_transform != null)
            _transform.position += _direction * _speed * Time.deltaTime;

    }

    /// <summary>
    /// Bounce the enemy if it collides with lava.
    /// Hurt/kill the enemy if it hits a bullet
    /// </summary>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Lava"))
            _direction = Vector2.Reflect(_direction, (collision.transform.position - _transform.position).normalized);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            Bullet bullet = collision.gameObject.GetComponent<Bullet>();

            if (bullet != null)
            {
                if (_currentHealth - bullet.Damage < 0 && bullet.Owner is Player)
                    bullet.Owner.UpdateScore();

                TakeDamage(bullet.Damage);
                Destroy(bullet.gameObject);
            }
        }
        else if (collision.gameObject.CompareTag("Enemy"))
            _direction = Vector2.Reflect(_direction, (collision.transform.position - _transform.position).normalized);
    }
    #endregion

    #region Health
    /// <summary>
    /// Hurt the enemy with the given damage
    /// </summary>
    /// <param name="damage">amount of damage to receive</param>
    public void TakeDamage(int damage)
    {
        _currentHealth -= damage;
        StartCoroutine(ShowDamage());

        if (_currentHealth < 0)
        {
            _currentHealth = 0;
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Flash the enemy's colour from red to white
    /// </summary>
    private IEnumerator ShowDamage()
    {
        if (_spriteRenderer != null)
        {
            _spriteRenderer.color = Color.red;

            yield return new WaitForSeconds(0.1f);

            _spriteRenderer.color = Color.white;
        }
    }
    #endregion
}