using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour
{
    #region Fields
    /// <summary>
    /// Reference to the bullet transform
    /// </summary>
    private Transform _transform;
    #endregion

    #region Inspector Fields
    /// <summary>
    /// Inspector field to how fast this bullet will go
    /// </summary>
    [SerializeField]
    private float _speed = 8;

    /// <summary>
    /// Inspector field for the amount of damage on contact
    /// </summary>
    [SerializeField]
    private int _damage = 10;
    #endregion

    #region Properties
    /// <summary>
    /// Gets or Sets the direction that the bullet will travel
    /// </summary>
    public Vector3 Direction { get; set; }

    /// <summary>
    /// Gets or Sets the amount of damage for this bullet
    /// </summary>
    public int Damage { get { return _damage; } set { _damage = value; } }

    /// <summary>
    /// Gets or sets the Owner of the bullet
    /// </summary>
    public Player Owner { get; set; }
    #endregion

    #region Behaviour Override
    /// <summary>
    /// Cache off our transform
    /// </summary>
    private void Awake()
    {
        _transform = transform;
    }

    /// <summary>
    /// Update the bullets position over time
    /// </summary>
    private void Update()
    {
        if (_transform != null)
            _transform.position += Direction * _speed * Time.deltaTime;
    }
    #endregion
}