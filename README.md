# Welcome to Las Vegas Unity3D Meetup Bitbucket Repo! #

## Latest Projects: ##
### 07.09.2015 Debugging ###
[UnityPackage](https://bitbucket.org/LasVegasUnityMeetup/meetupprojects/src/d69b38e5f8b22a3e4988d32e1ca5fd1c70e370fa/07.09.2015_Debugging/Assets/07.09.2015Debugging.unitypackage?at=master)

[Assets](https://bitbucket.org/LasVegasUnityMeetup/meetupprojects/src/d69b38e5f8b22a3e4988d32e1ca5fd1c70e370fa/07.09.2015_Debugging/Assets/?at=master)

[Completed Project](https://bitbucket.org/LasVegasUnityMeetup/meetupprojects/src/d69b38e5f8b22a3e4988d32e1ca5fd1c70e370fa/07.09.2015_Debugging/CompletedProject/Debugging/?at=master)

### 08.15.2015 Programming 101 ###
[Worksheet](https://bitbucket.org/LasVegasUnityMeetup/meetupprojects/downloads/Worksheet)

[Completed Worksheet](https://bitbucket.org/LasVegasUnityMeetup/meetupprojects/src/2893364415d08330a6c1df9e0a0d654c7489214c/08.15.2015_Programming101/Programming101/?at=master)