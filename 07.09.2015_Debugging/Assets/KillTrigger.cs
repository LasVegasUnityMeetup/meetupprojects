using UnityEngine;
using System.Collections;

public class KillTrigger : MonoBehaviour
{
    #region Behaviour Override
    /// <summary>
    /// When something besides the player comes in contact, destroy it!
    /// </summary>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer != LayerMask.NameToLayer("Player"))
            Destroy(collision.gameObject);
    }
    #endregion
}