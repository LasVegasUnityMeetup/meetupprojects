using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DebugMenu : MonoBehaviour
{
    #region Fields
    /// <summary>
    /// If the debug menu is showing or hidden
    /// </summary>
    private bool _showDebug;

    /// <summary>
    /// Reference to the canvas group to hide and display the controls
    /// </summary>
    private CanvasGroup _canvasGroup;
    #endregion

    #region Inspector Fields
    /// <summary>
    /// Inspector reference to the player
    /// </summary>
    [SerializeField]
    private Player _player;

    /// <summary>
    /// Inspector reference to the the enemy prefab
    /// </summary>
    [SerializeField]
    private Enemy _enemyPrefab;

    /// <summary>
    /// Inspector reference to the enemy spawn location
    /// </summary>
    [SerializeField]
    private Transform _enemySpawnLocation;

    /// <summary>
    /// Inspector reference to the god mode button
    /// </summary>
    [SerializeField]
    private Button _godModeButton;

    /// <summary>
    /// Inspector reference to the full health button
    /// </summary>
    [SerializeField]
    private Button _fullHealthButton;

    /// <summary>
    /// Inspector reference to the disable enemy button
    /// </summary>
    [SerializeField]
    private Button _disableEnemiesButton;

    /// <summary>
    /// Inspector reference to the spawn enemies button
    /// </summary>
    [SerializeField]
    private Button _spawnEnemiesButton;

    /// <summary>
    /// Inspector reference to the increase bullet damage button
    /// </summary>
    [SerializeField]
    private Button _increaseBulletDamageButton;
    #endregion

    #region Behaviour Override
    /// <summary>
    /// Validate Inspector fields and assign other members of the class
    /// </summary>
    private void Awake()
    {
        if (_player == null)
            Debug.LogError("Player reference not set! GameObject: " + name);

        if (_enemyPrefab == null)
            Debug.LogError("Enemy Prefab reference not set! GameObject: " + name);

        if (_enemySpawnLocation == null)
            Debug.LogError("Enemy spawn location reference not set! GameObject: " + name);

        if (_godModeButton != null)
            _godModeButton.onClick.AddListener(OnGodModeClicked);
        else
            Debug.LogError("God Mode button reference not set! GameObject: " + name);

        if (_fullHealthButton != null)
            _fullHealthButton.onClick.AddListener(OnFullHealthClicked);
        else
            Debug.LogError("Full health button reference not set! GameObject: " + name);

        if (_disableEnemiesButton != null)
            _disableEnemiesButton.onClick.AddListener(OnDisableEnemiesClicked);
        else
            Debug.LogError("Full health button reference not set! GameObject: " + name);

        if (_spawnEnemiesButton != null)
            _spawnEnemiesButton.onClick.AddListener(OnSpawnEnemiesClicked);
        else
            Debug.LogError("Full health button reference not set! GameObject: " + name);

        if (_increaseBulletDamageButton != null)
            _increaseBulletDamageButton.onClick.AddListener(OnIncreaseBulletDamage);
        else
            Debug.LogError("Increase bullet damage button reference not set! GameObject: " + name);

        _showDebug = false;
        _canvasGroup = GetComponent<CanvasGroup>();
    }

    /// <summary>
    /// Unhook from all the button events
    /// </summary>
    private void OnDestroy()
    {
        if (_godModeButton != null)
            _godModeButton.onClick.RemoveListener(OnGodModeClicked);

        if (_fullHealthButton != null)
            _fullHealthButton.onClick.RemoveListener(OnFullHealthClicked);

        if (_disableEnemiesButton != null)
            _disableEnemiesButton.onClick.RemoveListener(OnDisableEnemiesClicked);

        if (_spawnEnemiesButton != null)
            _spawnEnemiesButton.onClick.RemoveListener(OnSpawnEnemiesClicked);

        if (_increaseBulletDamageButton != null)
            _increaseBulletDamageButton.onClick.RemoveListener(OnIncreaseBulletDamage);
    }

    /// <summary>
    /// Check if we have press the debug button key
    /// </summary>
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && Debug.isDebugBuild)
        {
            _showDebug = !_showDebug;

            if (_canvasGroup != null)
            {
                _canvasGroup.alpha = _showDebug ? 1 : 0;
                _canvasGroup.interactable = _showDebug;
            }
        }
    }
    #endregion

    #region Events
    /// <summary>
    /// Handle when the god mode button is clicked
    /// </summary>
    private void OnGodModeClicked()
    {
        if (_player != null)
            _player.GodMode = !_player.GodMode;
    }

    /// <summary>
    /// Handle when the full health button is clicked
    /// </summary>
    private void OnFullHealthClicked()
    {
        if (_player != null)
            _player.SetFullHealth();
    }

    /// <summary>
    /// Handle when the disable enemies button is clicked
    /// </summary>
    private void OnDisableEnemiesClicked()
    {
        Enemy[] enemies = GameObject.FindObjectsOfType<Enemy>();

        if (enemies != null && enemies.Length > 0)
        {
            for (int i = 0; i < enemies.Length; ++i)
                enemies[i].enabled = !enemies[i].enabled;
        }
    }

    /// <summary>
    /// Handle when the on spawn enemies is clicked
    /// </summary>
    private void OnSpawnEnemiesClicked()
    {
        Enemy newEnemy = Instantiate<Enemy>(_enemyPrefab);

        if (newEnemy != null && _enemySpawnLocation != null)
            newEnemy.transform.position = _enemySpawnLocation.position;
    }

    /// <summary>
    /// Handle when the increase bullet damage is clicked
    /// </summary>
    private void OnIncreaseBulletDamage()
    {
        if (_player != null)
            _player.IncreaseBulletDamage();
    }
    #endregion
}